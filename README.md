# Badge system app
#### Simple web application for adding and visualizing earned badges for student's achievements during the studied course.
###### _starter point can be found in app.py_

### This app's tech stack contains:
* Python
* Flask
* SQLite3
* Jinja2
* CSS
* HTML

#Functionality of the application contains:
* Dropdown with all students + Dropdown with all badges hold inside the database.
* Submit button for inserting the desired data into the database.
* Table with 2 columns:
  * Student's name
  * Student's badges

## This application contains files for populating starter data into 3 tables:
* "students", which contains id + student's name : _can be found in file student_list.py_
* "badges", which contains id + badge's name : _can be found in file badges_list.py_
* "students_to_badges", which contains id references for what badge is assigned to which student : _can be found in file student_to_badges.py_
