import sqlite3
from logging import exception

from flask import Flask, render_template, request, g, redirect

app = Flask(__name__)
app.secret_key = "Sup3rCoMpl3xS3cr37k3y"


@app.route("/", methods=['GET', 'POST'])
def index():
    # index() - method for visualization of the badge system app.
    # on GET request return index.html view
    # on POST request submits form with new student-badge pair into students_to_badges table.
    if request.method == 'POST':
        try:
            data = request.form
            conn = get_db()
            cursor = conn.cursor()
            cursor.execute(
                f"INSERT INTO students_to_badges (student_id, badge_id) VALUES({data['student']}, {data['badge']})")
            cursor.close()
            conn.commit()
        except exception:
            return redirect("/", code=400)

    student_list = get_students()
    badges_list = get_badges()
    students_badges_dict = get_student_badges()
    return render_template('index.html', student_list=student_list, badges_list=badges_list,
                           students_badges_dict=students_badges_dict)


def get_db():
    # get_db() is help function for connection to the "badge_system" database.
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect('badge_system.db')
    return db


def close_connection():
    # close_connection() is help function for closing the connection to the database
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def get_students():
    # get_students is a method, which gets all students by their names from the database.
    # returns list.
    conn = get_db()
    cursor = conn.cursor()
    cursor.execute(" SELECT name, id FROM students ORDER BY name ")
    student_list = cursor.fetchall()
    return student_list


def get_badges():
    # get_badges is a method, which gets all badges by their names from the database.
    # returns list.
    conn = get_db()
    cursor = conn.cursor()
    cursor.execute(" SELECT name, id FROM badges ORDER BY name")
    badges_list = cursor.fetchall()
    return badges_list


def get_student_badges():
    # get_student_badges() is a method, which gets all students with their badges from the database.
    # returns dictionary.
    conn = get_db()
    cursor = conn.cursor()
    cursor.execute("""
                SELECT DISTINCT students.id, students.name, badges.id, badges.name, badges.color
                FROM students
                JOIN students_to_badges ON students.id = students_to_badges.student_id
                JOIN badges ON students_to_badges.badge_id = badges.id
    """)
    result_list = cursor.fetchall()
    students_badges_dict = {}

    # grouping all student's badges by student name
    for row in result_list:
        if row[1] not in students_badges_dict:
            students_badges_dict[row[1]] = []
        students_badges_dict[row[1]].append((row[3], row[4]))
    return students_badges_dict


if __name__ == '__main__':
    # method for starting the application
    app.run()
