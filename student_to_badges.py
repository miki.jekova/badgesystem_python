import sqlite3
# Run this if you want to import data for students_to_badges table in badge_system.db

connections = [
    ["1", "1"],
    ["1", "2"],
    ["1", "4"],
    ["3", "1"],
    ["24", "2"],
    ["34", "2"],
    ["39", "1"],
    ["3", "6"],
    ["5", "2"],
    ["5", "6"],
    ["8", "7"],
    ["10", "1"],
    ["22", "8"],
    ["45", "2"],
    ["66", "4"]
]

connection = sqlite3.connect("badge_system.db")
cursor = connection.cursor()

cursor.execute("""CREATE TABLE IF NOT EXISTS students_to_badges (
                    student_id INTEGER NOT NULL, badge_id INTEGER NOT NULL, 
                    FOREIGN KEY (student_id) REFERENCES students (id),
                    FOREIGN KEY (badge_id) REFERENCES badges (id),
                    PRIMARY KEY (student_id, badge_id)
                );""")

cursor.executemany("INSERT INTO students_to_badges(student_id, badge_id) VALUES(?,?)", connections)
print("added ", connections)

connection.commit()
connection.close()
