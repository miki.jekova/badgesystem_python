import sqlite3
# Run this if you want to import data for badges table in badge_system.db

badges = [
    "1 homework",
    "2 homework",
    "3 homework",
    "4 homework",
    "5 homework",
    "6 homework",
    "7 homework",
    "8 homework",
    "9 homework",
    "1 best HW",
    "2 best HW",
    "3 best HW",
    "4 best HW",
    "5 best HW",
    "6 best HW",
    "7 best HW",
    "8 best HW",
    "9 best HW",
    "1 lesson",
    "2 lesson",
    "3 lesson",
    "4 lesson",
    "5 lesson",
    "6 lesson",
    "7 lesson",
    "8 lesson",
    "9 lesson"
]

colors = [
    "#ffd6a5",
    "#fdffb6",
    "#caffbf",
    "#9bf6ff",
    "#a0c4ff",
    "#bdb2ff",
    "#ffc6ff",
    "#eae4e9",
    "#fff1e6",
    "#fde2e4",
    "#fad2e1",
    "#e2ece9",
    "#bee1e6",
    "#f0efeb",
    "#dfe7fd",
    "#cddafd",
    "#fbf8cc",
    "#fde4cf",
    "#ffcfd2",
    "#f1c0e8",
    "#cfbaf0",
    "#a3c4f3",
    "#90dbf4",
    "#8eecf5",
    "#98f5e1",
    "#b9fbc0"
]

badges = sorted(badges)

connection = sqlite3.connect("badge_system.db")
cursor = connection.cursor()
cursor.execute("CREATE TABLE IF NOT EXISTS badges (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, color TEXT)")
for i in range(len(badges)-1):
    cursor.execute("INSERT INTO badges (name, color) VALUES (?, ?)", [badges[i], colors[i]])
    print("added %s with color %s", badges[i], colors[i])

connection.commit()
connection.close()
